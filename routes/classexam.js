const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const Class = require('../models/classexam');

router.post('/create-classexam', (req, res, next) => {
    let newClass = new Class({
        ExamListId: req.body.ExamListId,
        Code: req.body.Code,
        TypeRoom: req.body.TypeRoom,
        RandomQuestion: req.body.RandomQuestion,
        RandomAnswer: req.body.RandomAnswer,
        Users: req.body.Users,
    });
    Class.addClass(newClass, (err, newClass) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: 'added' })
        }
    });
});

router.put('/update-classexam/:id', (req, res, next) => {
    Class.updateOne({'_id' : req.params.id},
        {
            $push: {
                Users: [req.body.Users],
            }
        },        
        {
            new: true
        },
        function (err, updated) {
            if (err) {
                res.json({ success: false, msg: 'failed to add' })
            } else {
                res.json({ success: true, msg: 'added' })
            }
        }
    )
}
);

router.get('/getclass-code/:id', (req, res) => {
    Class.findOne({ 'Code': req.params.id }, (err, result) => {
        if (err) throw err;
        if (result) {
            res.json(result)
        } else {
            res.send(JSON.stringify({
                error: 'Error'
            }))
        }
    })
});

module.exports = router;