const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const Examlist = require('../models/exam');

router.post('/create-examset', (req, res, next) => {
    let newExamSet = new Examlist({
        title: req.body.title,
        Subject: req.body.Subject,
        Teacher: req.body.Teacher,
        rate: req.body.rate,
        Point: req.body.Point,
        timecount: req.body.timecount,
        Examlist: req.body.Examlist,
    });
    Examlist.addExamlist(newExamSet, (err, examlist) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: 'added' })
        }
    });
});

router.get('/getall-examset/:id', (req, res) => {
    Examlist.find({ 'Subject': req.params.id }, (err, result) => {
        if (err) throw err;
        if (result) {
            res.json(result)
        } else {
            res.send(JSON.stringify({
                error: 'Error'
            }))
        }
    })
});

router.delete('/delete-examset/:id', (req, res) => {
    try {
        Examlist.findByIdAndDelete(req.params.id)
            .exec()
            .then(doc => {
                if (!doc) { return res.status(404).end(); }
                return res.status(204).end();
            }).catch(err => next(err));
    } catch { }
})

router.get('/get-examset/:id', async (req, res) => {
    await Examlist.findOne({ '_id': req.params.id }, (err, result) => {
        if (err) throw err;
        if (result) {
            res.json(result)
        } else {
            res.send(JSON.stringify({
                error: 'Error'
            }))
        }
    })
});

router.put('/update-examset/:id', (req, res, next) => {
    Examlist.findByIdAndUpdate(req.params.id,
        {
            $set: {
                title: req.body.title,
                Subject: req.body.Subject,
                Teacher: req.body.Teacher,
                rate: req.body.rate,
                Point: req.body.Point,
                timecount: req.body.timecount,
                Examlist: req.body.Examlist,
            }
        },
        {
            new: true
        },
        function (err, updated) {
            if (err) {
                res.json({ success: false, msg: 'failed to add' })
            } else {
                res.json({ success: true, msg: 'added' })
            }
        }
    )
}
);


module.exports = router;