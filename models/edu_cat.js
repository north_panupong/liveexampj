const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

const EducationCategorySchema = mongoose.Schema({
    Education_category: {
        type: String,
    },
    GradeLevel: {
        type: String,
    },
    
});

const EduCate = module.exports = mongoose.model('edu_category', EducationCategorySchema);

module.exports.getSubjectById = function (id, callback) {
    EduCate.findById(id, callback);
};

module.exports.newEduCate = function (newEduCate, callback) {
    newEduCate.save(callback);
};

