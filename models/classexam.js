const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

const ClassSchema = mongoose.Schema({
    ExamListId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    Code: {
        type:String,
    },
    TypeRoom: {
        type: String,
    },
    RandomQuestion: {
        type: Boolean,
        default: false
    },
    time: {
        type: Date,
        default: Date.now
    },
    RandomAnswer: {
        type: Boolean,
    },
    Users:{
        type: Array,
    }
});

const ClassExam = module.exports = mongoose.model('classExam', ClassSchema);

module.exports.getClassById = function (id, callback) {
    ClassExam.findById(id, callback);
};

module.exports.addClass = function (newClass, callback) {
    newClass.save(callback);
};
