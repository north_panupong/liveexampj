const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

const GallerySchema = mongoose.Schema({
    id: {
        type: String
    },
    imgUrl: {
        type: String,
    },
    imgTitle: {
        type: String,
    },
    imgDesc: {
        type: String,
        
    },
    uploaded: {
        type: Date,
        default: Date.now
    },
});

const Gallery = module.exports = mongoose.model('Gallery', GallerySchema);


