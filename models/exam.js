const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

const ExamSchema = mongoose.Schema({
    title: {
        type: String
    },
    Subject: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    Teacher: {
        type: mongoose.Schema.Types.ObjectId,
    },
    rate: {
        type: Number,
        default: 0
    },
    time: {
        type: Date,
        default: Date.now
    },
    Point: {
        type: Number,
    },
    timecount: {
        type: Number,
    },
    Examlist:{
        type: Object,
    },
    
});

const Examlist = module.exports = mongoose.model('examlist', ExamSchema);

module.exports.getExamById = function (id, callback) {
    Examlist.findById(id, callback);
};

module.exports.addExamlist = function (newExamSet, callback) {
    newExamSet.save(callback);
};

