import { TestBed } from '@angular/core/testing';

import { ExamsetService } from './examset.service';

describe('ExamsetService', () => {
  let service: ExamsetService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExamsetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
