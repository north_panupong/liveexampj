import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authToken: any;
  user: any;
  Domain = "https://node-api-vercel-mauve.vercel.app/";

  constructor(
    private http: HttpClient
  ) { }

  registerUser(user) {
    return this.http.post('http://localhost:3000/users/register', user)
  };

  checkUser(user){
    return this.http.post('http://localhost:3000/users/checkusername', user)
  }

  authenticateUser(user) {
    return this.http.post('http://localhost:3000/users/authenticate', user)
  }

  storageUser(token, user) {
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  getProfile() {
    this.loadToken();
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', this.authToken);
    //headers = headers.set('Content-Type','application/json');
    console.log('token', this.authToken);
    return this.http.get('http://localhost:3000/users/profile', { headers: headers });
  }

  loadToken() {
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

  checkpassword(user){
    return this.http.post('http://localhost:3000/users/checkpassword', user)
  }

  getUserByID(id:String){
    return this.http.get('http://localhost:3000/users/getUserByID/'+id);
  }


}
