import { Injectable } from '@angular/core';
import { HttpClient,HttpParams  } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ClassExamService {
  newClassExam:any;

  constructor(
    private http: HttpClient
    ) { }

  CreateClassExam(newClassExam) {
    return this.http.post('http://localhost:3000/classexam/create-classexam', newClassExam);
  };

  Update(id,list){
    return this.http.put('http://localhost:3000/classexam/update-classexam/'+id,list);
  }

  GetExamByCode(id:String){
    return this.http.get('http://localhost:3000/classexam/getclass-code/'+id);
  }

}

