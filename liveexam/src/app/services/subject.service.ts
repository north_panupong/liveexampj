import { Injectable } from '@angular/core';
import { HttpClient,HttpParams  } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class SubjectService {

  constructor(
    private http: HttpClient
  ) { }

  CreateSub(newSubject) {
    return this.http.post('http://localhost:3000/subjects/create-subject', newSubject);
  };

  GetAll(){
    return this.http.get('http://localhost:3000/subjects/getall-sub');
  }

  GetSubByTID(id:String){
    return this.http.get('http://localhost:3000/subjects/getall-byid/'+id);
  }

  Delete(id){
    return this.http.delete('http://localhost:3000/subjects/delete-subject/'+id);
  }
  DeleteMany(id){
    return this.http.delete('http://localhost:3000/subjects/delete-subjectByEdu/'+id);
  }
  Update(id,list){
    return this.http.put('http://localhost:3000/subjects/update-subject/'+id,list);
  }
}
