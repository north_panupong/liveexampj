import { TestBed } from '@angular/core/testing';

import { ClassExamService } from './class-exam.service';

describe('ClassExamService', () => {
  let service: ClassExamService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ClassExamService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
