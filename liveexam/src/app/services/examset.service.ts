import { Injectable } from '@angular/core';
import { HttpClient,HttpParams  } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ExamsetService {
  newExamSet:any;

  constructor(
    private http: HttpClient
  ) { }

  CreateExamlist(newExamSet) {
    return this.http.post('http://localhost:3000/exams/create-examset', newExamSet);
  };

  GetExamListByTID(id:String){
    return this.http.get('http://localhost:3000/exams/getall-examset/'+id);
  }

  Delete(id){
    return this.http.delete('http://localhost:3000/exams/delete-examset/'+id);
  }

  GetExamByID(id:String){
    return this.http.get('http://localhost:3000/exams/get-examset/'+id);
  }

  Update(id,list){
    return this.http.put('http://localhost:3000/exams/update-examset/'+id,list);
  }

  /*GetExamByCode(id:String){
    return this.http.get('http://localhost:3000/exams/getcode-examset/'+id);
  }*/

}

