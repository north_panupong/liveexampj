import { Injectable } from '@angular/core';
import { HttpClient,HttpParams  } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class EducateService {

  constructor(private http: HttpClient) { }

  CreateEduCate(newEduCate) {
    return this.http.post('http://localhost:3000/educates/create-Edu', newEduCate);
  };

  GetAll(){
    return this.http.get('http://localhost:3000/educates/getall-Edu');
  }

  Delete(id){
    return this.http.delete('http://localhost:3000/educates/delete-educate/'+id);
  }
  GetEduByTID(id:String){
    return this.http.get('http://localhost:3000/educates/getall-byid/'+id);
  }
  Update(id,list){
    return this.http.put('http://localhost:3000/educates/update-educate/'+id,list);
  }
}
