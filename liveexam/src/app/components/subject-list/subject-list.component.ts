import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { ActivatedRoute,Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ModalCreateSubjectComponent } from '../modal-create-subject/modal-create-subject.component';
import { SubjectService } from '../../services/subject.service';
import {ModalDeleteSubjectComponent} from '../modal-delete-subject/modal-delete-subject.component';
import {ModalEditSubjectComponent} from '../modal-edit-subject/modal-edit-subject.component';
interface test {
  title: string;
  id:String;
}

@Component({
  selector: 'app-subject-list',
  templateUrl: './subject-list.component.html',
  styleUrls: ['./subject-list.component.css']
})
export class SubjectListComponent implements OnInit {

  idEducate:String;
  page=1;
  pageSize=15;
  count: number;
  Sublist:any;

  constructor(private _location: Location,
    private route: ActivatedRoute,
    private router: Router,
    public modalService: NgbModal,
    public subjectService: SubjectService,) { }

  ngOnInit(): void {
    this.idEducate = this.route.snapshot.paramMap.get('id');
    this.getAll();
    console.log(this.Sublist);
  }

  backClicked() {
    this._location.back();
  }
  goExamList(id){
    this.router.navigate(['/exam-list',id]);
  }

  getAll(){
    this.subjectService.GetSubByTID(this.idEducate).subscribe(
      (param: any[]) => {
        this.Sublist = param;
        if (param.length) {
          this.count = param.length;
        } else {
          this.count = 0;
        }
      });
  }

  openModal() {
    const modalRef = this.modalService.open(ModalCreateSubjectComponent);
    modalRef.componentInstance.id = this.idEducate;
  }

  Delete(id,title){
    const modalRef = this.modalService.open(ModalDeleteSubjectComponent);
    modalRef.componentInstance.id = id;
    modalRef.componentInstance.title = title;
    /*this.educateService.Delete(id).subscribe( );
    window.location.reload();*/
  }

  edit(list){
    const modalRef = this.modalService.open(ModalEditSubjectComponent);
    modalRef.componentInstance.list = list;
  }

}
