import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user: Object;
  name: String;
  lastname: String;
  email: String;
  role: String;

  rolethai:String;


  constructor(
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.authService.getProfile().subscribe((profile: any) => {
      this.user = profile.user;
      this.name = profile.user.name;
      this.email = profile.user.email;
      this.lastname = profile.user.lastname;
      this.role = profile.user.role;
      if(profile.user.role == "student"){
        this.rolethai = "นักเรียน";
      }
      if(profile.user.role == "teacher"){
        this.rolethai = "อาจารย์";
      }
    },
      err => {
        console.log(err);
        return false;
      });
  }

}
