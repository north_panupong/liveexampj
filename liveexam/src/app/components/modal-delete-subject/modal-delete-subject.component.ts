import { Component, OnInit,Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SubjectService } from '../../services/subject.service';

@Component({
  selector: 'app-modal-delete-subject',
  templateUrl: './modal-delete-subject.component.html',
  styleUrls: ['./modal-delete-subject.component.css']
})
export class ModalDeleteSubjectComponent implements OnInit {

  constructor(
    public activeModal: NgbActiveModal,
    public subjectService: SubjectService
  ) { }

  ngOnInit(): void {
  }

  DeleteSubject(){
    this.subjectService.Delete(this.id).subscribe( );
    window.location.reload();
  }

  @Input() public id;
  @Input() public title;

}
