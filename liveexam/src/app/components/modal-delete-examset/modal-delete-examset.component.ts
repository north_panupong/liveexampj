import { Component, OnInit,Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EducateService } from '../../services/educate.service';
import { SubjectService } from '../../services/subject.service';

@Component({
  selector: 'app-modal-delete-examset',
  templateUrl: './modal-delete-examset.component.html',
  styleUrls: ['./modal-delete-examset.component.css']
})

export class ModalDeleteExamsetComponent implements OnInit {

  title:String;
  dataEdu:any;

  constructor(public activeModal: NgbActiveModal,
    public educateService: EducateService,
    public subjectService: SubjectService,) { }

  ngOnInit(): void {
    this.getData();
  }

  DeleteEducat(){
    this.educateService.Delete(this.id).subscribe( );
    this.subjectService.DeleteMany(this.dataEdu._id).subscribe();
    window.location.reload();
  }

  getData(){
    this.educateService.GetEduByTID(this.id).subscribe((param: any[]) => {

      this.dataEdu = param;
      console.log(this.dataEdu);
      this.title = this.dataEdu.Education_category +" "+this.dataEdu.GradeLevel;
    });

  }

  @Input() public id;

}
