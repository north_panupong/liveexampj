import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDeleteExamsetComponent } from './modal-delete-examset.component';

describe('ModalDeleteExamsetComponent', () => {
  let component: ModalDeleteExamsetComponent;
  let fixture: ComponentFixture<ModalDeleteExamsetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalDeleteExamsetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDeleteExamsetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
