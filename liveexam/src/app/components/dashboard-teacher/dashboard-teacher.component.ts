import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ModalCreateExamsetComponent } from '../modal-create-examset/modal-create-examset.component';
import {ModalDeleteExamsetComponent} from '../modal-delete-examset/modal-delete-examset.component';
import {ModalEditExamsetComponent} from '../modal-edit-examset/modal-edit-examset.component';
import { Router } from '@angular/router';
import { EducateService } from '../../services/educate.service';
import { from } from 'rxjs';
interface Order {
  value: string;
}

@Component({
  selector: 'app-dashboard-teacher',
  templateUrl: './dashboard-teacher.component.html',
  styleUrls: ['./dashboard-teacher.component.css']
})
export class DashboardTeacherComponent implements OnInit {

  orders: Order[] = [
    { value: 'ล่าสุด' },
    { value: 'เก่าสุด' }
  ];

  user: any;
  id: any;
  examlist: any;
  count: number;

  page=1;
  pageSize=15;

  constructor(public modalService: NgbModal,
    private router: Router,
    public educateService: EducateService,
  ) { }

  ngOnInit(): void {
    this.getIDTeach();
  }

  openVideoPopup() {
    const modalRef = this.modalService.open(ModalCreateExamsetComponent);
  }


  getID() {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.id = this.user.id;
  }

  Delete(id){
    const modalRef = this.modalService.open(ModalDeleteExamsetComponent);
    modalRef.componentInstance.id = id;
    /*this.educateService.Delete(id).subscribe( );
    window.location.reload();*/
  }

  edit(list){
    const modalRef = this.modalService.open(ModalEditExamsetComponent);
    modalRef.componentInstance.list = list;
  }

  async getIDTeach() {
    await this.getID();
    await this.Getall();

  }

  Getall() {
    console.log('id test', this.id);
    this.educateService.GetAll().subscribe(
      (param: any[]) => {
        console.log('param',param);
        this.examlist = param;
        if (param.length) {
          this.count = param.length;
        } else {
          this.count = 0;
        }
      });
  }
  goListSub(id){
    this.router.navigate(['/subjectlist',id]);
  }

}
