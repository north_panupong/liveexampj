import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: String;
  password: String;

  constructor(
    private router: Router,
    private authService: AuthService,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit(): void {
  }

  onLoginSubmit() {
    const user = {
      username: this.username,
      password: this.password
    }
    console.log('user', user);

    this.authService.authenticateUser(user).subscribe((data: any) => {
      if (data.success) {
        this.authService.storageUser(data.token, data.user);
        this.router.navigate(['/']).then(() => {
          window.location.reload();
        });

      } else {
        this.flashMessage.show('ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง', { cssClass: 'alert-danger', timeout: 3000 });
      }
    });
  }

  GotoRegister() {
    this.router.navigate(['/register']).then(() => {
      window.location.reload();
    });
  }
}
