import { Component, OnInit,Input} from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ExamsetService } from 'src/app/services/examset.service';

@Component({
  selector: 'app-modal-delete-examlist',
  templateUrl: './modal-delete-examlist.component.html',
  styleUrls: ['./modal-delete-examlist.component.css']
})
export class ModalDeleteExamlistComponent implements OnInit {
  title:String;
  dataEdu:any;

  constructor(public activeModal: NgbActiveModal,
    public examsetService: ExamsetService,
    ) { }

  ngOnInit(): void {
  }
  DeleteExam(){
    this.examsetService.Delete(this.id).subscribe( );
    window.location.reload();
  }
  @Input() public id;
}
