import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDeleteExamlistComponent } from './modal-delete-examlist.component';

describe('ModalDeleteExamlistComponent', () => {
  let component: ModalDeleteExamlistComponent;
  let fixture: ComponentFixture<ModalDeleteExamlistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalDeleteExamlistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDeleteExamlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
