import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css']
})
export class SettingComponent implements OnInit {

  user: Object;
  name: String;
  lastname: String;
  email: String;
  username: String;

  constructor(
    private authService: AuthService,
    ) { }

  ngOnInit(): void {
    this.getuser();
  }

  getuser(){
    this.authService.getProfile().subscribe((profile: any) => {
      this.user = profile.user;
      this.name = profile.user.name;
      this.email = profile.user.email;
      this.lastname = profile.user.lastname;
      this.username = profile.user.username;
    });
    console.log(this.user);
  }

}
