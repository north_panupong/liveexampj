import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditExamsetComponent } from './edit-examset.component';

describe('EditExamsetComponent', () => {
  let component: EditExamsetComponent;
  let fixture: ComponentFixture<EditExamsetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditExamsetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditExamsetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
