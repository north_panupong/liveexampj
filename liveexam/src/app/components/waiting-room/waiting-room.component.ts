import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-waiting-room',
  templateUrl: './waiting-room.component.html',
  styleUrls: ['./waiting-room.component.css']
})
export class WaitingRoomComponent implements OnInit {

  profileImg = false;
  text1 = "";
  text2 = "";
  text3 = "";
  text4 = "";

  constructor() { }

  ngOnInit(): void {

    setTimeout(() => {
      this.profileImg = true;
      this.text1 = "นายไม้เอก ไม้โท";
      this.text2 = "Heart Surgoen at Some Hospital";
      this.text3 = "Phone - +1 1234567890 Email - john.doe@example.com";
      this.text4 =
        "lorem isopem oowieurl laksh oweir oha oasdhakjsdhiuwreyh uahi uasddhiaus";
    }, 3000);

  }

}
