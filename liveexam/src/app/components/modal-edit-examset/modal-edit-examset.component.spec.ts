import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEditExamsetComponent } from './modal-edit-examset.component';

describe('ModalEditExamsetComponent', () => {
  let component: ModalEditExamsetComponent;
  let fixture: ComponentFixture<ModalEditExamsetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalEditExamsetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEditExamsetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
