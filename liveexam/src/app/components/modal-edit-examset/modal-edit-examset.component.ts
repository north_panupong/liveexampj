import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EducateService } from '../../services/educate.service';
import { MatSnackBar } from '@angular/material/snack-bar';

interface Level {
  value: string;
}
interface PNum {
  value: string;
}

@Component({
  selector: 'app-modal-edit-examset',
  templateUrl: './modal-edit-examset.component.html',
  styleUrls: ['./modal-edit-examset.component.css']
})

export class ModalEditExamsetComponent implements OnInit {

  @Input() public list;

  title: String = "";

  Education_category: any;
  Teacher: String;
  GradeLevel: String;
  user: any;
  sub: any;

  titlesub: String;
  des: String;
  level: Level[] = [
    { value: 'ประถมศึกษา' },
    { value: 'มัธยมศึกษา' },
    { value: 'อุดมศึกษา' },
  ];

  pnum: PNum[] = [
    { value: '1' },
    { value: '2' },
    { value: '3' },
    { value: '4' },
    { value: '5' },
    { value: '6' },
  ];

  levelselect: String;

  constructor(
    public activeModal: NgbActiveModal,
    public educateService: EducateService,
    private _snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
    console.log(this.list);
  }

  openSnackBar() {
    this._snackBar.open("updated", "", {
      duration: 2000,
    });
  }

  async Update() {
    if (this.list.Education_category == "ประถมศึกษา" || this.list.Education_category == "มัธยมศึกษา") {
      const list = {
        id: this.list._id,
        GradeLevel: this.list.GradeLevel,
        Education_category: this.list.Education_category,
      }
      this.educateService.Update(this.list._id, list).subscribe((data: any) => {
        if (data.success) {
          console.log("success");
          this.openSnackBar();
          this.activeModal.dismiss();
          setTimeout(() => {
            window.location.reload()
           }, 2000);
        }
        else {
          console.log("not success");
        }
      });
    }
    if (this.list.Education_category == "อุดมศึกษา") {
      const list = {
        id: this.list._id,
        GradeLevel: this.title,
        Education_category: this.list.Education_category,
      }
      await this.educateService.Update(this.list._id, list).subscribe((data: any) => {
        console.log(data);
        if (data.success) {
          console.log("success");
          this.openSnackBar();
          this.activeModal.dismiss();
          setTimeout(() => {
          window.location.reload()
         }, 2000);

        }
        else {
          console.log("not success");
        }
      });
    }
  }

}
