import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { ExamsetService } from '../../services/examset.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { param } from 'jquery';
import { AuthService } from '../../services/auth.service';
import { ModalDeleteExamlistComponent } from '../modal-delete-examlist/modal-delete-examlist.component'

interface test {
  title: string;
}

@Component({
  selector: 'app-exam-list',
  templateUrl: './exam-list.component.html',
  styleUrls: ['./exam-list.component.css']
})
export class ExamListComponent implements OnInit {
  idExamList:String;
  page=1;
  pageSize=15;
  count: number;
  tests:test[] = [{title:"gogo"}]
  examList:any;
  T:any;

  constructor(private _location: Location,
    private route: ActivatedRoute,
    private router: Router,
    public examService: ExamsetService,
    private authService: AuthService,
    public modalService: NgbModal,) { }

  ngOnInit(): void {
    this.idExamList = this.route.snapshot.paramMap.get('id');
    this.getAll()
  }

  backClicked() {
    this._location.back();
  }
  goToCreateExam(id){
    this.router.navigate(['/create-exam',id]);
  }

  goToEditExam(id){
    this.router.navigate(['/deit-examset',id]);
  }
  /*getAll = async()=>{
    const getex = await this.getExamlist();
    const result = console.log(getex);
    return result;
  }*/

  getAll(){
    this.examService.GetExamListByTID(this.idExamList).subscribe(
      (param: any[]) => {
        this.examList = param;
        if (param.length) {
          this.count = param.length;
        } else {
          this.count = 0;
        }
      });
  }

  goCreateRoom(id){
    this.router.navigate(['/create-room',id]);
  }

  Delete(id,title){
    const modalRef = this.modalService.open(ModalDeleteExamlistComponent);
    modalRef.componentInstance.id = id;
    modalRef.componentInstance.title = title;
    /*this.educateService.Delete(id).subscribe( );
    window.location.reload();*/
  }

}
