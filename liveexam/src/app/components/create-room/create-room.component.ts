import { Component, OnInit } from '@angular/core';
import { ExamsetService } from '../../services/examset.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { ClipboardService } from 'ngx-clipboard';
import { ClassExamService } from '../../services/class-exam.service';
import arrayShuffle from 'array-shuffle';

@Component({
  selector: 'app-create-room',
  templateUrl: './create-room.component.html',
  styleUrls: ['./create-room.component.css']
})
export class CreateRoomComponent implements OnInit {

  examlist: any;
  id: String;
  code = "";
  TypeRoom = "Instant Feedback";
  RandomQ: boolean = false;
  RandomA: boolean = false;
  textModal: string;
  isCopied1: boolean;
  User:any;

  idExamList:any;

  newList:any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public examService: ExamsetService,
    private _clipboardService: ClipboardService,
    public classExam: ClassExamService,

  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getExamList();
    this.makeid();
    this.User = JSON.parse(localStorage.getItem("user"));

    this._clipboardService.copyResponse$.subscribe(re => {
      if (re.isSuccess) {
        alert('copy success!');
      }
    });

    this.idExamList = this.route.snapshot.paramMap.get('id');
  }

  getExamList() {
    this.examService.GetExamByID(this.id).subscribe(
      (param: any[]) => {
        this.examlist = param;
        if (param.length) {
          console.log("success");
        } else {
          console.log("error");
        }
      });
  }

  makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    this.code = text;

  }

  onChange(e) {
    this.TypeRoom = e.target.value;
    console.log(this.TypeRoom);
  }

  callServiceToCopy() {
    this._clipboardService.copy('This is copy thru service copyFromContent directly');
  }

  onCopyFailure() {
    alert('copy fail!');
  }

  goToWaitingRoom() {
    this.createExamSet();
    this.router.navigate(['/waiting', this.code]);
  }

  createExamSet() {
    const newclass = {
      ExamListId: this.idExamList,
      Code: this.code,
      TypeRoom: this.TypeRoom,
      RandomQuestion: this.RandomQ,
      RandomAnswer: this.RandomA,
      Users:this.User.id,
    }
    console.log(newclass);
    this.classExam.CreateClassExam(newclass).subscribe((data: any) => {
      if (data.success) {
        console.log("success");
      }
      else {
        console.log("not success");
      }
    });
  }

  // Random array list
  /*RandomList(){
    this.newList = arrayShuffle(this.examlist.Examlist);
    console.log(this.newList);
  }*/

}
