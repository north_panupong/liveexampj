import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ExamsetService } from '../../services/examset.service';
import { SubjectService } from '../../services/subject.service';
import { EducateService } from '../../services/educate.service';
import { Router } from '@angular/router';

interface Level {
  value: string;
}
interface PNum {
  value: string;
}


@Component({
  selector: 'app-modal-create-examset',
  templateUrl: './modal-create-examset.component.html',
  styleUrls: ['./modal-create-examset.component.css']
})
export class ModalCreateExamsetComponent implements OnInit {
  CheckedValue = false;

  title: String = "";

  Education_category: any;
  Teacher: String;
  GradeLevel :String;
  user: any;
  sub: any;

  titlesub: String;
  des: String;
  level: Level[] = [
    { value: 'ประถมศึกษา' },
    { value: 'มัธยมศึกษา' },
    { value: 'อุดมศึกษา' },
  ];

  pnum: PNum[] = [
    { value: '1' },
    { value: '2' },
    { value: '3' },
    { value: '4' },
    { value: '5' },
    { value: '6' },
  ];

  levelselect: String;


  @Input() src;
  constructor(
    public activeModal: NgbActiveModal,
    public examService: ExamsetService,
    public subjectService: SubjectService,
    public educateService: EducateService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.Teacher = this.user.id;
    this.GetAllSub();
  }

  checkaddclass() {
    this.CheckedValue = !this.CheckedValue;
    console.log(this.CheckedValue);
  }

  createExamlist() {
    if (this.Education_category == "ประถมศึกษา"||this.Education_category == "มัธยมศึกษา") {
      const newEduCate = {
        GradeLevel: this.GradeLevel,
        Education_category: this.Education_category,
      }
      this.educateService.CreateEduCate(newEduCate).subscribe((data: any) => {
        if (data.success) {
          console.log("success");
          this.activeModal.dismiss();
          this.GotoRegister();
        }
        else {
          console.log("not success");
        }
      });
    }
    if (this.Education_category == "อุดมศึกษา") {
      const newEduCate = {
        GradeLevel: this.title,
        Education_category: this.Education_category,
      }
      this.educateService.CreateEduCate(newEduCate).subscribe((data: any) => {
        if (data.success) {
          console.log("success");
          this.activeModal.dismiss();
          this.GotoRegister();
        }
        else {
          console.log("not success");
        }
      });
    }
  }

  GotoRegister() {
    this.router.navigate(['/dash-teacher']).then(() => {
      window.location.reload();
    });
  }

  GetAllSub() {
    this.subjectService.GetAll().subscribe(
      (listsub: any[]) => {
        console.log(listsub);
        this.sub = listsub;
      });
  }

}
