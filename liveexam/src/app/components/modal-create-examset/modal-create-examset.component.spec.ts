import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCreateExamsetComponent } from './modal-create-examset.component';

describe('ModalCreateExamsetComponent', () => {
  let component: ModalCreateExamsetComponent;
  let fixture: ComponentFixture<ModalCreateExamsetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalCreateExamsetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCreateExamsetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
