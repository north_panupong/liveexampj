import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  token:any;
  constructor() { }

  ngOnInit(): void {
  }
  getToken() {
    this.token = localStorage.getItem('id_token');
  }
  ngDoCheck(){
    this.getToken();
  }

}
