import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';


@Component({
  selector: 'app-joinroom',
  templateUrl: './joinroom.component.html',
  styleUrls: ['./joinroom.component.css']
})
export class JoinroomComponent implements OnInit {

  code:String;
  id:String;
  class:any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    ) { }

  ngOnInit(): void {

  }

  goToWaitingRoom() {
    this.router.navigate(['/doexam', this.code]);
  }



}
