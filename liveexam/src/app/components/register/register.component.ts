import { Component, OnInit } from '@angular/core';
import { ValidationService } from '../../services/validation.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { ElementRef, ViewChild, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  name: String;
  lasname: String;
  username: String;
  password: String;
  email: String;
  role: String;
  isValue: number = 0;

  show: boolean = true;
  pass: boolean = false;

  constructor(
    private ValidationService: ValidationService,
    private flashMessage: FlashMessagesService,
    private authService: AuthService,
    private router: Router) { }

  ngOnInit(): void {
  }
  toggle1() { this.isValue = 1; this.pass = true; }
  toggle2() { this.isValue = 2; this.pass = true; }

  Togglehide() {
    this.show = !this.show;
  }

  teacher() {
    this.role = "teacher";
  }

  student() {
    this.role = "student";
  }

  onRegisterSubmit() {
    const user = {
      name: this.name,
      lastname: this.lasname,
      email: this.email,
      username: this.username,
      password: this.password,
      role: this.role,
    }

    console.log(user);

    // Required Fields
    if (!this.ValidationService.validateRegister(user)) {
      this.flashMessage.show('กรุณากรอกข้อมูลให้ครบทุกช่อง', { cssClass: 'alert-danger', timeout: 3000 });
      return false;
    }

    this.authService.checkUser(user).subscribe((data: any) => {
      if (data.success) {
        this.authService.registerUser(user).subscribe((data: any) => {
          if (data.success) {
            this.flashMessage.show('You are now registered and can log in', { cssClass: 'alert-success', timeout: 3000 });
            setTimeout(() => {
              this.router.navigate(['/login']);
            },
              3000);
          } else {
            this.flashMessage.show('Something went wrong', { cssClass: 'alert-danger', timeout: 3000 });
            this.router.navigate(['/register']);
          }
        });

      } else {
        this.flashMessage.show('มีชื่อผู้ใช้นี้อยู่แล้ว', { cssClass: 'alert-danger', timeout: 3000 });
        return false;
      }
    });

    // Validate Email
    if (!this.ValidationService.validateEmail(user.email)) {
      this.flashMessage.show('โปรดระบุ email ให้ถูกต้อง', { cssClass: 'alert-danger', timeout: 3000 });
      return false;
    }

  }



}
