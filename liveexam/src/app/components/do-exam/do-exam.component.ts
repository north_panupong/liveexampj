import { Component, OnInit } from '@angular/core';
import { ExamsetService } from '../../services/examset.service';
import { ActivatedRoute } from '@angular/router';
import { ClassExamService } from '../../services/class-exam.service';

interface examlist {
  no:Number;
  question: any;
  type: string;
  answer: any;
  point: Number;
  Studentanswer:any;
}

@Component({
  selector: 'app-do-exam',
  templateUrl: './do-exam.component.html',
  styleUrls: ['./do-exam.component.css']
})
export class DoExamComponent implements OnInit {
  code: String;
  id: String;
  class: any;
  examID: String;

  examSet: any;
  title: String;
  idExamList: any;
  Teacher: any;
  elist: examlist[] = [];
  timecount: any;
  rate: any;
  count: number;

  page=1;
  pageSize=1;

  constructor(public examService: ExamsetService,
    private route: ActivatedRoute,
    public classExam: ClassExamService) { }

  async ngOnInit() {
    this.id = await this.route.snapshot.paramMap.get('code');
    this.getExam();
  }


  getExam() {
    this.classExam.GetExamByCode(this.id).subscribe(
      (data: any[]) => {
        this.class = data;
        this.examID = this.class.ExamListId;
        this.examService.GetExamByID(this.class.ExamListId).subscribe(
          (param: any[]) => {
            this.examSet = param;
            this.title = this.examSet.title;
            this.idExamList = this.examSet.Subject;
            this.Teacher = this.examSet.Teacher;
            this.elist = this.examSet.Examlist;
            this.timecount = this.examSet.timecount;
            this.rate = this.examSet.rate;
            this.count = this.examSet.Examlist.length;
          });
      });
  }

  checkcount(){
    this.count = this.elist.length;
    console.log(this.elist);
  }

  getExamSet(id) {
    this.getExam();
    this.examService.GetExamByID(id).subscribe(
      (param: any[]) => {
        this.examSet = param;
        this.title = this.examSet.title;
        this.idExamList = this.examSet.Subject;
        this.Teacher = this.examSet.Teacher;
        this.elist = this.examSet.Examlist;
        this.timecount = this.examSet.timecount;
        this.rate = this.examSet.rate;
        this.count = this.examSet.Examlist.length;
      });
  }

}
