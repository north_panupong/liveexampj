import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { Token } from '@angular/compiler/src/ml_parser/lexer';
import * as $ from 'jquery';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  token: any;


  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.getToken();
  }
  getToken() {
    this.token = localStorage.getItem('id_token')
  }
  togglebtn() {


    $("#wrapper").toggleClass("toggled");

    console.log("toggle");
  }
  Gotohome() {
    this.router.navigate(['/']).then(() => {
      window.location.reload();
    });
  }

  goRegister(){
    this.router.navigate(['./register'])
  }
  goLogin(){
    this.router.navigate(['./login'])
  }

}
