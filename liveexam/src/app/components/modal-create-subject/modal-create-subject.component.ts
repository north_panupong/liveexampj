import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SubjectService } from '../../services/subject.service';
import { AuthService } from '../../services/auth.service';
@Component({
  selector: 'app-modal-create-subject',
  templateUrl: './modal-create-subject.component.html',
  styleUrls: ['./modal-create-subject.component.css']
})
export class ModalCreateSubjectComponent implements OnInit {

  code: String;
  name: String;
  teacher: any;
  nameTeach: String;

  constructor(public activeModal: NgbActiveModal,
    public subjectService: SubjectService,
    private authService: AuthService,) { }

  @Input() public id;

  ngOnInit(): void {
    this.authService.getProfile().subscribe((profile: any) => {
      this.teacher = profile.user.name +" "+ profile.user.lastname;
    },
      err => {
        console.log(err);
        return false;
      });
  }

  createSubject() {
    const newSub = {
      codeSubject: this.code,
      nameSubject: this.name,
      Teacher: this.teacher,
      idEdu: this.id,
    }
    this.subjectService.CreateSub(newSub).subscribe((data: any) => {
      if (data.success) {
        console.log("success");
        window.location.reload();
        this.activeModal.dismiss();
      }
      else {
        console.log("not success");
      }
    });
  }

}
