import { Component, OnInit, Input } from '@angular/core';
import { SubjectService } from '../../services/subject.service';
import { AuthService } from '../../services/auth.service';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-modal-edit-subject',
  templateUrl: './modal-edit-subject.component.html',
  styleUrls: ['./modal-edit-subject.component.css']
})
export class ModalEditSubjectComponent implements OnInit {

  code: String;
  name: String;
  teacher: any;
  nameTeach: String;
  id: String;


  @Input() public list;

  constructor(
    public activeModal: NgbActiveModal,
    public subjectService: SubjectService,
    private authService: AuthService,
    private _snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
    this.getname();
    this.getData();
    console.log(this.list);
  }

  getname() {
    this.authService.getProfile().subscribe((profile: any) => {
      this.teacher = profile.user.name + " " + profile.user.lastname;
    },
      err => {
        console.log(err);
        return false;
      });
  }

  getData() {
    this.code = this.list.codeSubject;
    this.name = this.list.nameSubject;
    this.teacher = this.list.Teacher;
  }

  async Update() {

    const list = {
      codeSubject: this.code,
      nameSubject: this.name,
      Teacher: this.teacher,
    }
    this.subjectService.Update(this.list._id, list).subscribe((data: any) => {
      if (data.success) {
        console.log("success");
        this.openSnackBar();
        this.activeModal.dismiss();
        setTimeout(() => {
          window.location.reload()
        }, 2000);
      }
      else {
        console.log("not success");
      }
    });
  }

  openSnackBar() {
    this._snackBar.open("updated", "", {
      duration: 2000,
    });
  }

}
