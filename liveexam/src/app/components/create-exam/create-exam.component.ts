import { Component, OnInit } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { Router } from '@angular/router';
import { ExamsetService } from '../../services/examset.service';
import { ActivatedRoute } from '@angular/router';

interface examlist {
  no:Number;
  question: any;
  type: string;
  answer: any;
  point: Number;
}


@Component({
  selector: 'app-create-exam',
  templateUrl: './create-exam.component.html',
  styleUrls: ['./create-exam.component.css']
})

export class CreateExamComponent implements OnInit {

  htmlContent = '';
  elist: examlist[] = [];

  idExamList: String;
  user: any;

  page = 1;
  pageSize = 1;
  Point: Number = 0;

  timecount = '';
  rate = '';
  title = '';
  numno = 1;

  a1: examlist;

  count: number;
  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '10rem',
    minHeight: '5rem',
    placeholder: 'คำถาม...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['bold']
    ]
  };

  constructor(
    private router: Router,
    public examService: ExamsetService,
    private route: ActivatedRoute,) { }

  ngOnInit(): void {
    this.testadd();
    this.idExamList = this.route.snapshot.paramMap.get('id');
    this.user = JSON.parse(localStorage.getItem("user"));
  }

  testadd() {
    this.a1 = {
      no:this.numno,
      question: "",
      type: "อัตนัยแบบสั้น",
      answer: [{
        answer: "",
        check: false
      }],
      point: 0
    };
    this.elist.push(this.a1);
    this.count = this.elist.length;
    this.numno+=1;
    console.log(this.elist);
    console.log("count", this.count);
  }
  removeadd() {
    this.elist.splice(-1);
    this.count = this.elist.length;
    this.numno-=1;
  }

  addForm(i) {
    this.elist[i].answer.push({
      answer: ' ',
      check: false
    });
    console.log(this.a1);

  }

  removeForm(i) {
    this.elist[i].answer.splice(-1);
    console.log(this.a1);
  }

  reset(listtype: String) {
    switch (listtype) {
      case "อัตนัยแบบสั้น":
        this.a1.answer.splice(0, this.a1.answer.length - 1);
        console.log("answer length" + this.a1.answer.length);
        break;
      case "อัตนัยแบบยาว":
        this.a1.answer.splice(0, this.a1.answer.length - 1);
        console.log("answer length" + this.a1.answer.length);
        break;
    }
  }

  createExamSet() {

    this.getPoint();

    const newExamSet = {
      title: this.title,
      Subject: this.idExamList,
      Teacher: this.user.id,
      rate: this.rate,
      Point: this.Point,
      timecount: this.timecount,
      Examlist: this.elist,
    }
    console.log(newExamSet);
    this.examService.CreateExamlist(newExamSet).subscribe((data: any) => {
      if (data.success) {
        console.log("success");
      }
      else {
        console.log("not success");
      }
    });
  }

  getPoint() {
    let num: Number = 0;
    for (let i = 0; i < this.elist.length; i++) {
      this.Point = this.Point.valueOf() + this.elist[i].point.valueOf();
    }
    console.log(typeof this.Point)
  }

}
