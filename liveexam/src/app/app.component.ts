import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: "liveexam";
  user: Object;
  name: String;
  lastname: String;
  email: String;
  role: String;
  checkteach:boolean;

  token: any;

  rolethai:String;

  valuemenu:number = 0;

  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.getProfile();
    this.getToken();
    console.log("check");
  }

  onLogoutClick() {
    this.authService.logout();
    this.router.navigate(['/']).then(() => {
      this.reset();
      window.location.reload();
    });
  }
  getToken() {
    this.token = localStorage.getItem('id_token');
  }
  reset() {
    this.user = null;
    this.name = null;
    this.email = null;
    this.lastname = null;
    this.role = null;
  }

  getProfile() {
    this.authService.getProfile().subscribe((profile: any) => {
      this.user = profile.user;
      this.name = profile.user.name;
      this.email = profile.user.email;
      this.lastname = profile.user.lastname;
      this.role = profile.user.role;
      if(profile.user.role == "student"){
        this.rolethai = "นักเรียน";
        this.checkteach = true;
      }
      if(profile.user.role == "teacher"){
        this.rolethai = "อาจารย์";
        this.checkteach = false;
      }
    },
      err => {
        console.log(err);
        return false;
      });
  }

  toggle1() { this.valuemenu = 1;  }
  toggle2() { this.valuemenu = 2;  }
  toggle3() { this.valuemenu = 3;  }
  toggle4() { this.valuemenu = 4;  }

}
