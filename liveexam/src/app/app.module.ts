import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule,Routes} from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { FormsModule } from '@angular/forms';
import {ValidationService} from './services/validation.service';
import {AuthService} from './services/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { PriScrollbarModule } from 'pri-ng-scrollbar';
import { CreateExamComponent } from './components/create-exam/create-exam.component';
import { DashboardTeacherComponent } from './components/dashboard-teacher/dashboard-teacher.component';
import { ReportComponent } from './components/report/report.component';
import { ViewReportComponent } from './components/view-report/view-report.component';
import { SettingComponent } from './components/setting/setting.component';
import { CreateRoomComponent } from './components/create-room/create-room.component';
import { WaitingRoomComponent } from './components/waiting-room/waiting-room.component';
import { JoinroomComponent } from './components/joinroom/joinroom.component';
import { DoExamComponent } from './components/do-exam/do-exam.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { NgxHideOnScrollModule } from 'ngx-hide-on-scroll';
import { ClipboardModule } from 'ngx-clipboard';


import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatSelectModule} from '@angular/material/select';
import { ExamListComponent } from './components/exam-list/exam-list.component';
import { ModalCreateExamsetComponent } from './components/modal-create-examset/modal-create-examset.component';
import { CommonModule } from '@angular/common';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { SubjectListComponent } from './components/subject-list/subject-list.component';
import { ModalDeleteExamsetComponent } from './components/modal-delete-examset/modal-delete-examset.component';
import { ModalCreateSubjectComponent } from './components/modal-create-subject/modal-create-subject.component';
import { ModalDeleteSubjectComponent } from './components/modal-delete-subject/modal-delete-subject.component';
import { ModalEditExamsetComponent } from './components/modal-edit-examset/modal-edit-examset.component';
import { ModalEditSubjectComponent } from './components/modal-edit-subject/modal-edit-subject.component';
import { EditExamsetComponent } from './components/edit-examset/edit-examset.component';
import { ModalDeleteExamlistComponent } from './components/modal-delete-examlist/modal-delete-examlist.component';

const appRoutes : Routes = [
  {path:'', component : HomeComponent},
  {path:'register', component : RegisterComponent},
  {path:'login', component : LoginComponent},
  {path:'profile', component : ProfileComponent},
  {path:'create-exam/:id', component : CreateExamComponent},
  {path:'dash-teacher', component : DashboardTeacherComponent},
  {path:'report', component : ReportComponent},
  {path:'view-report', component : ViewReportComponent},
  {path:'setting', component : SettingComponent},
  {path:'create-room/:id', component : CreateRoomComponent},
  {path:'waiting/:code', component : WaitingRoomComponent},
  {path:'join-room', component : JoinroomComponent},
  {path:'doexam/:code', component : DoExamComponent},
  {path:'exam-list/:id', component : ExamListComponent},
  {path:'subjectlist/:id', component : SubjectListComponent},
  {path:'deit-examset/:id', component : EditExamsetComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProfileComponent,
    CreateExamComponent,
    DashboardTeacherComponent,
    ReportComponent,
    ViewReportComponent,
    SettingComponent,
    CreateRoomComponent,
    WaitingRoomComponent,
    JoinroomComponent,
    DoExamComponent,
    ExamListComponent,
    ModalCreateExamsetComponent,
    SubjectListComponent,
    ModalDeleteExamsetComponent,
    ModalCreateSubjectComponent,
    ModalDeleteSubjectComponent,
    ModalEditExamsetComponent,
    ModalEditSubjectComponent,
    EditExamsetComponent,
    ModalDeleteExamlistComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    FlashMessagesModule.forRoot(),
    FormsModule,
    HttpClientModule,
    PriScrollbarModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    MatSelectModule,
    NgbModule,
    CommonModule,
    AngularEditorModule,
    MatSnackBarModule,
    NgxHideOnScrollModule,
    ClipboardModule,
  ],
  providers: [
    ValidationService,
    AuthService],
  bootstrap: [
    AppComponent
  ],
  entryComponents:[
    ModalCreateExamsetComponent,
    ModalDeleteExamsetComponent,
    ModalCreateSubjectComponent,
    ModalDeleteSubjectComponent,
    ModalEditExamsetComponent,
    ModalEditSubjectComponent,
    ModalDeleteExamlistComponent
  ],
  exports:[
    CommonModule,
    FormsModule
  ]
})
export class AppModule { }
