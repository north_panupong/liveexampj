**Technology Stack**
- Angular
- MongoDB
- NodeJs
- ExpressJs
- SocketIo
- webrtc using simple-peer

**Install Environment**

install nodejs

    https://nodejs.org/en/

install angular Cli

    npm install -g @angular/cli

**Install Project**

Server

    -- npm install

Client

    -- cd./liveexam

    -- npm install

**Run Project**

Server

    -- nodemon

Client

    -- cd./liveexam

    -- ng serve